/*******************************************************************************
 * Copyright (c) 2015 BSI Business Systems Integration AG.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BSI Business Systems Integration AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.scout.sdk.core.fixture;

import java.math.RoundingMode;

/**
 *
 */
public @interface AnnotationWithArrayValues {
  public int[] nums();

  public RoundingMode[] enumValues();

  public String[] strings();

  public Class<?>[] types();

  public AnnotationWithSingleValues[] annos();
}
