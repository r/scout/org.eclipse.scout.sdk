/*******************************************************************************
 * Copyright (c) 2015 BSI Business Systems Integration AG.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BSI Business Systems Integration AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.scout.sdk.core.model.spi.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;
import org.eclipse.jdt.internal.compiler.lookup.BlockScope;
import org.eclipse.jdt.internal.compiler.lookup.ClassScope;
import org.eclipse.jdt.internal.compiler.lookup.Scope;
import org.eclipse.jdt.internal.compiler.lookup.TypeBinding;
import org.eclipse.scout.sdk.core.model.api.ISourceRange;
import org.eclipse.scout.sdk.core.model.api.ITypeParameter;
import org.eclipse.scout.sdk.core.model.api.internal.TypeParameterImplementor;
import org.eclipse.scout.sdk.core.model.spi.CompilationUnitSpi;
import org.eclipse.scout.sdk.core.model.spi.JavaElementSpi;
import org.eclipse.scout.sdk.core.model.spi.MemberSpi;
import org.eclipse.scout.sdk.core.model.spi.TypeParameterSpi;
import org.eclipse.scout.sdk.core.model.spi.TypeSpi;

/**
 * <h3>{@link DeclarationTypeParameterWithJdt}</h3>
 *
 * @author Ivan Motsch
 * @since 5.1.0
 */
public class DeclarationTypeParameterWithJdt extends AbstractJavaElementWithJdt<ITypeParameter> implements TypeParameterSpi {
  private final AbstractMemberWithJdt<?> m_declaringMember;
  private final org.eclipse.jdt.internal.compiler.ast.TypeParameter m_astNode;
  private final int m_index;
  private final String m_name;
  private List<TypeSpi> m_bounds;
  private ISourceRange m_source;

  DeclarationTypeParameterWithJdt(JavaEnvironmentWithJdt env, AbstractMemberWithJdt<?> declaringMember, org.eclipse.jdt.internal.compiler.ast.TypeParameter astNode, int index) {
    super(env);
    m_declaringMember = Validate.notNull(declaringMember);
    m_astNode = Validate.notNull(astNode);
    m_index = index;
    m_name = new String(m_astNode.name);
  }

  @Override
  protected JavaElementSpi internalFindNewElement(JavaEnvironmentWithJdt newEnv) {
    MemberSpi newMember = (MemberSpi) getDeclaringMember().internalFindNewElement(newEnv);
    if (newMember != null && newMember.getTypeParameters().size() > m_index) {
      return newMember.getTypeParameters().get(m_index);
    }
    return null;
  }

  @Override
  protected ITypeParameter internalCreateApi() {
    return new TypeParameterImplementor(this);
  }

  public org.eclipse.jdt.internal.compiler.ast.TypeParameter getInternalTypeParameter() {
    return m_astNode;
  }

  @Override
  public List<TypeSpi> getBounds() {
    if (m_bounds != null) {
      return m_bounds;
    }

    boolean hasType = m_astNode.type != null;
    boolean hasBounds = m_astNode.bounds != null && m_astNode.bounds.length > 0;
    int size = 0;
    if (hasType) {
      size++;
    }
    if (hasBounds) {
      size += m_astNode.bounds.length;
    }
    List<TypeSpi> result = new ArrayList<>(size);
    Scope scope = SpiWithJdtUtils.memberScopeOf(this);
    if (hasType) {
      TypeSpi t = SpiWithJdtUtils.bindingToType(m_env, ensureResolvedType(scope, m_astNode.type));
      if (t != null) {
        result.add(t);
      }
    }
    if (hasBounds) {
      for (TypeReference r : m_astNode.bounds) {
        TypeSpi t = SpiWithJdtUtils.bindingToType(m_env, ensureResolvedType(scope, r));
        if (t != null) {
          result.add(t);
        }
      }
    }
    m_bounds = Collections.unmodifiableList(result);
    return m_bounds;
  }

  protected TypeBinding ensureResolvedType(Scope scope, TypeReference r) {
    TypeBinding resolvedType = r.resolvedType;
    if (resolvedType != null) {
      return resolvedType;
    }

    if (scope instanceof ClassScope) {
      r.resolveType((ClassScope) scope);
    }
    else {
      r.resolveType((BlockScope) scope);
    }
    return r.resolvedType;
  }

  @Override
  public AbstractMemberWithJdt<?> getDeclaringMember() {
    return m_declaringMember;
  }

  @Override
  public String getElementName() {
    return m_name;
  }

  @Override
  public ISourceRange getSource() {
    if (m_source == null) {
      CompilationUnitSpi cu = SpiWithJdtUtils.declaringTypeOf(this).getCompilationUnit();
      org.eclipse.jdt.internal.compiler.ast.TypeParameter decl = m_astNode;
      m_source = m_env.getSource(cu, decl.declarationSourceStart, decl.declarationSourceEnd);
    }
    return m_source;
  }
}
