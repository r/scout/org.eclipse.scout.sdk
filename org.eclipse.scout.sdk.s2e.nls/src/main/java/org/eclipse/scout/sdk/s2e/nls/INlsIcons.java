/*******************************************************************************
 * Copyright (c) 2015 BSI Business Systems Integration AG.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     BSI Business Systems Integration AG - initial API and implementation
 ******************************************************************************/
package org.eclipse.scout.sdk.s2e.nls;

/**
 * <h4>INlsIcons</h4>
 *
 * @author Andreas Hoegger
 * @since 1.1.0 (30.11.2010)
 */
public interface INlsIcons {
  String REFRESH = "refresh";
  String EXPORT = "export";
  String MAGNIFIER = "magnifier";
  String WARNING = "warning8";
  String COMMENT = "comment";
  String IMPORT = "import";
  String FILE_ADD_PENDING = "fileadd_pending";
  String TEXT_REMOVE = "text_remove";
  String TEXT = "text";
  String TEXT_ADD = "text_add";
  String FIND_OBJECT = "find_obj";
}
