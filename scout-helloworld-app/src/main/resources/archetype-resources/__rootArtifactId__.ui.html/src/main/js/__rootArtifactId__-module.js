/**
 * Include your custom JavaScript files here.
 */
(function(${rootArtifactId}, scout, $, undefined) {
  // Sample:
  // __include("${rootArtifactId}/myCustomJs/myCustom.js");
}(window.${rootArtifactId} = window.${rootArtifactId} || {}, scout, jQuery));
